#include <iostream>
#include <string>
#include <math.h>
#include <mutex>


#include "ros/ros.h"
#include "eigen3/Eigen/Eigen"
#include <binders.h>

//robot process
#include "robot_process.h"

//pnp pose subsriber
#include "solvepnp/FramePoses.h"

//droneMsgROS
#include "droneMsgsROS/dronePose.h"

//EKF-SLAM localizer
#include "Kalman_Loc2.h"

//reference frames
#include "referenceFramesROS.h"

//Eigen
#include "eigen3/Eigen/Eigen"

//tf
#include "tf_conversions/tf_eigen.h"

//opencv
#include "opencv2/opencv.hpp"

//map pose vector
#include "frame_localizer_process/MapPoses.h"

class frame_localizer_process_ros : public RobotProcess
{

public:
    frame_localizer_process_ros();
    ~frame_localizer_process_ros();

public:
    void ownSetUp();
    void ownStart();
    void ownStop();
    void ownRun();
    void init();

private:
    Kalman_Loc localization_;

    bool received_odom_data_;
    bool received_first_odom_data_;
    bool received_pnp_obs_data_;

    float x_odom_;
    float y_odom_;
    float z_odom_;
    float roll_odom_;
    float pitch_odom_;
    float yaw_odom_;

    float x_orig_, y_orig_, z_orig_;
    float roll_orig_, pitch_orig_, yaw_orig_;


    std::vector<Observation3D> pnp_obs_vec_;
private:
    std::string stack_path_;
    std::string config_file_;
    std::string drone_id_;
    std::string odom_pose_;
    std::string pnp_pose_;
    std::string frame_localizer_pose_;
    std::string map_vector_;
    std::string current_map_vector_;

protected:
    void transformCamframeToRobotframe(Eigen::Matrix4d &transformation_mat, double x, double y, double z, Eigen::Quaterniond quat);

private:
    ros::NodeHandle n;

    //Subscibers
    ros::Subscriber odom_pose_sub_;
    void odomPoseCallback(const droneMsgsROS::dronePose& msg);

    ros::Subscriber pnp_pose_sub_;
    void pnpPoseCallback(const solvepnp::FramePoses& msg);

    //Publishers
    ros::Publisher frame_localizer_pose_pub_;
    inline void publishPose(droneMsgsROS::dronePose pose);

    ros::Publisher map_vector_pub_;
    inline void publishMapVector();

    ros::Publisher current_map_vector_pub_;
    inline void publishCurrentMapVector();

};





