#include "frame_localizer.h"

frame_localizer_process_ros::frame_localizer_process_ros():RobotProcess()
{
    std::cout << "frame_localizer constructor " << std::endl;
}

frame_localizer_process_ros::~frame_localizer_process_ros()
{
    std::cout << "frame_localizer destructor " << std::endl;
}


void frame_localizer_process_ros::ownSetUp()
{
    //assign all the variables here
    received_odom_data_       = false;
    received_first_odom_data_ = false;
    received_pnp_obs_data_    = false;
    x_odom_     = 0;
    y_odom_     = 0;
    z_odom_     = 0;
    roll_odom_  = 0;
    pitch_odom_ = 0;
    yaw_odom_   = 0;

    //initializing the filter
    this->init();
}

void frame_localizer_process_ros::ownStop()
{
    //assign all the variables to default here
    received_odom_data_       = false;
    received_first_odom_data_ = false;
    received_pnp_obs_data_    = false;
    x_odom_     = 0;
    y_odom_     = 0;
    z_odom_     = 0;
    roll_odom_  = 0;
    pitch_odom_ = 0;
    yaw_odom_   = 0;

}

void frame_localizer_process_ros::ownRun()
{
    //run the algorithm over here
    bool new_estimated_pose = false;

    if(received_odom_data_)
    {
        //std::cout << "X_odom " << x_odom_ << "y odom " << y_odom_ << "z odom " << z_odom_ << std::endl;
        localization_.KalmanPredict(x_odom_, y_odom_, z_odom_,
                                    roll_odom_, pitch_odom_, yaw_odom_);
        received_odom_data_ = false;
        new_estimated_pose  = true;
    }

    if(received_pnp_obs_data_)
    {
        localization_.KalmanUpdatePnP(pnp_obs_vec_);
        pnp_obs_vec_.clear();
        received_pnp_obs_data_ = false;
        new_estimated_pose     = true;
        this->publishCurrentMapVector();
        this->publishMapVector();
    }

    droneMsgsROS::dronePose estimated_pose;
    if(new_estimated_pose)
    {
        //std::cout << "I am ready to publish the filter prediction " << std::endl;
        estimated_pose.header.stamp = ros::Time::now();
        estimated_pose.x     = localization_.x;
        estimated_pose.y     = localization_.y;
        estimated_pose.z     = localization_.z;
        estimated_pose.roll  = localization_.yaw;
        estimated_pose.pitch = localization_.pitch;
        estimated_pose.yaw   = localization_.roll;
        publishPose(estimated_pose);
    }

}

void frame_localizer_process_ros::init()
{
    ros::param::get("~stack_path", stack_path_);
    if(stack_path_.length() == 0)
        stack_path_ = "~/workspace/ros/aerostack_catkin_ws/src/aerostack_stack";

    ros::param::get("~config_file", config_file_);
    if(config_file_.length() == 0)
        config_file_ = "params_localization_obs.xml";

    ros::param::get("~drone_id", drone_id_);

    ros::param::get("~odom_pose", odom_pose_);
    if(odom_pose_.length() == 0)
        odom_pose_ = "EstimatedPose_droneGMR_wrt_GFF";

    ros::param::get("~pnp_pose",pnp_pose_);
    if(pnp_pose_.length() == 0)
        pnp_pose_ = "/Re_PNP";

    ros::param::get("~frame_localizer_pose", frame_localizer_pose_);
    if(frame_localizer_pose_.length() == 0)
        frame_localizer_pose_ = "frame_localizer_estimated_pose";

    ros::param::get("~map_vector", map_vector_);
    if(map_vector_.length() == 0)
        map_vector_ = "gate_map_vector";

    ros::param::get("~current_map_vector", current_map_vector_);
    if(current_map_vector_.length() == 0)
        current_map_vector_ = "current_gate_map_vector";

    //std::cout << "droneId " << drone_id_ << std::endl;
    localization_.initialization(stack_path_+"/"+"configs/drone"+(drone_id_)+"/"+config_file_);
}

void frame_localizer_process_ros::ownStart()
{

    //start all the subscribers and publishers over here
    odom_pose_sub_ = n.subscribe(odom_pose_,1, &frame_localizer_process_ros::odomPoseCallback, this);
    pnp_pose_sub_  = n.subscribe(pnp_pose_, 1, &frame_localizer_process_ros::pnpPoseCallback, this);


    frame_localizer_pose_pub_ = n.advertise<droneMsgsROS::dronePose>(frame_localizer_pose_, 1);
    map_vector_pub_           = n.advertise<frame_localizer_process::MapPoses>(map_vector_,1);
    current_map_vector_pub_   = n.advertise<frame_localizer_process::MapPoses>(current_map_vector_, 1);
}

void frame_localizer_process_ros::pnpPoseCallback(const solvepnp::FramePoses &msg)
{
    if(msg.poses.size() == 0)
        return;

    pnp_obs_vec_.clear();
    for(int i=0; i < msg.poses.size(); ++i)
    {
        Eigen::Quaterniond pnp_quat;
        pnp_quat.x() = msg.poses[i].pose.orientation.x;
        pnp_quat.y() = msg.poses[i].pose.orientation.y;
        pnp_quat.z() = msg.poses[i].pose.orientation.z;
        pnp_quat.w() = msg.poses[i].pose.orientation.w;

        double x, y, z;
        x = msg.poses[i].pose.position.x;
        y = msg.poses[i].pose.position.y;
        z = msg.poses[i].pose.position.z;

        Eigen::Matrix4d transformation_mat;
        this->transformCamframeToRobotframe(transformation_mat, x, y, z, pnp_quat);

        tf::Quaternion q(msg.poses[i].pose.orientation.x, msg.poses[i].pose.orientation.y,
                         msg.poses[i].pose.orientation.z, msg.poses[i].pose.orientation.w);
        tf::Matrix3x3 m(q);
        //convert quaternion to euler angels
        double yaw, pitch, roll;
        m.getRPY(roll, pitch, yaw);

        //converting the roll, pitch and yaw in the world frame
        Eigen::Vector4d RPY_cam_frame, RPY_world_frame;
        RPY_cam_frame(0) = roll;
        RPY_cam_frame(1) = pitch;
        RPY_cam_frame(2) = yaw;
        RPY_cam_frame(3) = 1;

        RPY_world_frame = transformation_mat * RPY_cam_frame;
        //        std::cout << "roll in the world frame " << RPY_world_frame(0)
        //                  << "pitch in world frame " << RPY_world_frame(1)
        //                  << "yaw in world frame " << RPY_world_frame(2) << std::endl;

        //converting the x, y, z in the world frame
        Eigen::Vector4d position_cam_frame, position_world_frame;
        position_cam_frame(0) = x;
        position_cam_frame(1) = y;
        position_cam_frame(2) = z;
        position_cam_frame(3) = 1;

        position_world_frame = transformation_mat * position_cam_frame;

        //        std::cout << "X: " << position_world_frame(0)
        //                  << "Y: " << position_world_frame(1)
        //                  << "Z: " << position_world_frame(2) << std::endl;


        Observation3D pnp_obs;
        pnp_obs.gate_id = msg.frame_id;
        pnp_obs.x       = position_world_frame(0);
        pnp_obs.y       = position_world_frame(1);
        pnp_obs.z       = position_world_frame(2);
        pnp_obs.yaw     = RPY_world_frame(0);   // yaw is the roll for the SLAM lib
        pnp_obs.pitch   = RPY_world_frame(1);
        pnp_obs.roll    = RPY_world_frame(2);

        pnp_obs_vec_.push_back(pnp_obs);
    }

    received_pnp_obs_data_ = true;

}

void frame_localizer_process_ros::odomPoseCallback(const droneMsgsROS::dronePose &msg)
{
    x_orig_ = msg.x;
    y_orig_ = msg.y;
    z_orig_ = msg.z;
    roll_orig_ = msg.roll;
    pitch_orig_ = msg.pitch;
    yaw_orig_   = msg.yaw;

    droneMsgsROS::dronePose mobile_frame_msg;

    referenceFrames::wYvPuR2xYyPzR(msg, &mobile_frame_msg);
    x_odom_     = mobile_frame_msg.x;
    y_odom_     = mobile_frame_msg.y;
    z_odom_     = mobile_frame_msg.z;
    roll_odom_  = mobile_frame_msg.roll;
    pitch_odom_ = mobile_frame_msg.pitch;
    yaw_odom_   = mobile_frame_msg.yaw;

    //assinging the first odom pose to the previous prediction pose of the filter
    if(!received_first_odom_data_)
    {
        std::cout << "this is a check of for robot process start" << std::endl;
        localization_.prev_odom_pose(1) = x_odom_;
        localization_.prev_odom_pose(2) = y_odom_;
        localization_.prev_odom_pose(3) = z_odom_;
        localization_.prev_odom_pose(4) = roll_odom_;
        localization_.prev_odom_pose(5) = pitch_odom_;
        localization_.prev_odom_pose(6) = yaw_odom_;
        received_first_odom_data_ = true;
    }

    received_odom_data_ = true;

}

inline void frame_localizer_process_ros::publishPose(droneMsgsROS::dronePose pose)
{
    frame_localizer_pose_pub_.publish(pose);
}

void frame_localizer_process_ros::transformCamframeToRobotframe(Eigen::Matrix4d &transformation_mat, double x, double y, double z,
                                                                Eigen::Quaterniond quat)
{
    Eigen::Matrix4d rot_x_cam, rot_x_robot, rot_z_robot, translation_cam, T_robot_world, T_obj_cam, T_cam_world;
    rot_x_cam.setZero(4,4), rot_x_robot.setZero(4,4), rot_z_robot.setZero(4,4), translation_cam.setZero(4,4), T_robot_world.setZero(4,4),
            T_obj_cam.setZero(4,4), T_cam_world.setZero(4,4);


    //this is needed if the cam has a pitch angle
    //    rot_x_cam(0,0) = 1;
    //    rot_x_cam(1,1) =  cos(-slamdunk_pitch_angle);
    //    rot_x_cam(1,2) = -sin(-slamdunk_pitch_angle);
    //    rot_x_cam(2,1) =  sin(-slamdunk_pitch_angle);
    //    rot_x_cam(2,2) =  cos(-slamdunk_pitch_angle);
    //    rot_x_cam(3,3) = 1;

    //rotation of -90
    rot_x_robot(0,0) = 1;
    rot_x_robot(1,1) =  cos(-1.5708);
    rot_x_robot(1,2) = -sin(-1.5708);
    rot_x_robot(2,1) =  sin(-1.5708);
    rot_x_robot(2,2) =  cos(-1.5708);
    rot_x_robot(3,3) = 1;

    //rotation of -90
    rot_z_robot(0,0) = cos(-1.5708);
    rot_z_robot(0,1) = -sin(-1.5708);
    rot_z_robot(1,0) = sin(-1.5708);
    rot_z_robot(1,1) = cos(-1.5708);
    rot_z_robot(2,2) = 1;
    rot_z_robot(3,3) = 1;

    //transformation from robot to world
    T_robot_world(0,0) = cos(yaw_orig_)*cos(pitch_orig_);
    T_robot_world(0,1) = cos(yaw_orig_)*sin(pitch_orig_)*sin(roll_orig_) - sin(yaw_orig_)*cos(roll_orig_);
    T_robot_world(0,2) = cos(yaw_orig_)*sin(pitch_orig_)*cos(roll_orig_) + sin(yaw_orig_)*sin(pitch_orig_);

    T_robot_world(1,0) = sin(yaw_orig_)*cos(pitch_orig_);
    T_robot_world(1,1) = sin(yaw_orig_)*sin(pitch_orig_)*sin(roll_orig_) + cos(yaw_orig_)*cos(roll_orig_);
    T_robot_world(1,2) = sin(yaw_orig_)*sin(pitch_orig_)*cos(roll_orig_) - cos(yaw_orig_)*sin(roll_orig_);

    T_robot_world(2,0) = -sin(pitch_orig_);
    T_robot_world(2,1) = cos(pitch_orig_)*sin(roll_orig_);
    T_robot_world(2,2) = cos(pitch_orig_)*cos(roll_orig_);

    T_robot_world(0,3) = x_orig_;
    T_robot_world(1,3) = y_orig_;
    T_robot_world(2,3) = z_orig_;
    T_robot_world(3,3) = 1;

    //the rvec and tvec from pnp which object ins cam frame
    Eigen::Matrix3d Rmat_E;
    Rmat_E = quat.toRotationMatrix();

    T_obj_cam(0,0) = Rmat_E(0,0);
    T_obj_cam(0,1) = Rmat_E(0,1);
    T_obj_cam(0,2) = Rmat_E(0,2);
    T_obj_cam(0,3) = x;
    T_obj_cam(1,0) = Rmat_E(1,0);
    T_obj_cam(1,1) = Rmat_E(1,1);
    T_obj_cam(1,2) = Rmat_E(1,2);
    T_obj_cam(1,3) = y;
    T_obj_cam(2,0) = Rmat_E(2,0);
    T_obj_cam(2,1) = Rmat_E(2,1);
    T_obj_cam(2,2) = Rmat_E(2,2);
    T_obj_cam(2,3) = z;
    T_obj_cam(3,0) = 0;
    T_obj_cam(3,1) = 0;
    T_obj_cam(3,2) = 0;
    T_obj_cam(3,3) = 1;


    //add this if there is translation between cam and robot
    //    translation_cam(0,0) = 1;
    //    translation_cam(0,3) = -0.063;
    //    translation_cam(1,1) = 1;
    //    translation_cam(1,3) = -0.08;
    //    translation_cam(2,2) = 1;
    //    translation_cam(2,3) = -0.065;
    //    translation_cam(3,3) = 1;

    T_cam_world = /*T_robot_world **/ rot_z_robot * rot_x_robot;
    transformation_mat = T_cam_world /** T_obj_cam*/;
}

inline void frame_localizer_process_ros::publishMapVector()
{
    std::vector<Landmark3D> map = localization_.getMapVector();
    frame_localizer_process::MapPoses map_pose_vec;

    //    std::cout << "map  "  << map.x
    //              <<  " " << map.y
    //               << " " << map.z << std::endl;

    for(int i = 0; i < map.size(); ++i)
    {
        geometry_msgs::PoseStamped map_pose;
        map_pose.header.stamp = ros::Time::now();
        //frame_id in this case is the name of the gate
        map_pose.header.frame_id = map[i].gate_id;
        map_pose.pose.position.x = map[i].x;
        map_pose.pose.position.y = map[i].y;
        map_pose.pose.position.z = map[i].z;

        double roll, pitch, yaw;
        roll  = map[i].roll;
        pitch = map[i].pitch;
        yaw   = map[i].yaw;

        tf::Quaternion quaternion = tf::createQuaternionFromRPY(roll, pitch, yaw);

        map_pose.pose.orientation.x = quaternion.getX();
        map_pose.pose.orientation.y = quaternion.getY();
        map_pose.pose.orientation.z = quaternion.getZ();
        map_pose.pose.orientation.w = quaternion.getW();
        map_pose_vec.poses.push_back(map_pose);
    }

    map_vector_pub_.publish(map_pose_vec);

}

inline void frame_localizer_process_ros::publishCurrentMapVector()
{
    std::vector<Landmark3D> map = localization_.getCurrentMapVector();
    frame_localizer_process::MapPoses map_pose_vec;

    //    std::cout << "map  "  << map.x
    //              <<  " " << map.y
    //               << " " << map.z << std::endl;

    for(int i = 0; i < map.size(); ++i)
    {
        geometry_msgs::PoseStamped map_pose;
        map_pose.header.stamp = ros::Time::now();
        //frame_id in this case is the name of the gate
        map_pose.header.frame_id = map[i].gate_id;
        map_pose.pose.position.x = map[i].x;
        map_pose.pose.position.y = map[i].y;
        map_pose.pose.position.z = map[i].z;

        double roll, pitch, yaw;
        roll  = map[i].yaw; //this is not a bug
        pitch = map[i].pitch;
        yaw   = map[i].roll;

        tf::Quaternion quaternion = tf::createQuaternionFromRPY(roll, pitch, yaw);

        map_pose.pose.orientation.x = quaternion.getX();
        map_pose.pose.orientation.y = quaternion.getY();
        map_pose.pose.orientation.z = quaternion.getZ();
        map_pose.pose.orientation.w = quaternion.getW();
        map_pose_vec.poses.push_back(map_pose);
    }

    current_map_vector_pub_.publish(map_pose_vec);

}
