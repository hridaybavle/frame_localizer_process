#include "frame_localizer.h"

int main(int argc, char **argv)
{

    ros::init(argc, argv, ros::this_node::getName());

    frame_localizer_process_ros my_frame_localizer;
    my_frame_localizer.setUp();

    ros::Rate r(30);

    while(ros::ok())
    {
        //updating all the ros msgs
        ros::spinOnce();
        //running the localizer
        my_frame_localizer.run();
        r.sleep();
    }

    return 0;
}


